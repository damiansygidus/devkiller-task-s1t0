/*
   This file is provided exclusively for the purpose of Spartez Online Assessment.
   Unauthorized distribution of this file, derived work or information about its
   content, via any medium, is strictly prohibited.
 */
package findarray;

import java.util.Arrays;

public class MyFindArray implements FindArray {

	@Override
	public int findArray(int[] array, int[] subArray) {
		int subArrayIndex = -1;
		int[] tempArray;

		if (array != null && subArray != null && array.length > 0 && subArray.length > 0 &&
				subArray.length < array.length) {

			for (int i = array.length - subArray.length; i-- > 0;) {
				if (array[i] == subArray[0]) {
					tempArray = Arrays.copyOfRange(array, i, i + subArray.length);
					if (Arrays.equals(subArray, tempArray)) {
						subArrayIndex = i;
						break;
					}
				}
			}
		}
		return subArrayIndex;
	}
}